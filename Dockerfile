FROM ubuntu:16.04
RUN apt-get update -y \
    && apt-get -qqy dist-upgrade \
    && apt-get -qqy install software-properties-common gettext-base unzip \
	&& rm -rf /var/lib/apt/lists/* /var/cache/apt/*


RUN add-apt-repository ppa:webupd8team/java -y
RUN apt-get update -y
RUN add-apt-repository ppa:linuxuprising/java -y
RUN apt-get update -y
RUN add-apt-repository ppa:openjdk-r/ppa
RUN apt-get update -y
RUN apt-get install openjdk-11-jdk -y
RUN java -version


# Google Chromocker
# Install Chrome
RUN apt install wget
RUN apt update
RUN apt-get install gdebi -y
RUN apt-get update -y
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt install ./google-chrome-stable_current_amd64.deb -y
RUN google-chrome --version


# ChromeDriver
ARG CHROME_DRIVER_VERSION=85.0.4183.87
RUN wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip \
	&& rm -rf /opt/chromedriver \
	&& unzip /tmp/chromedriver_linux64.zip -d /opt \
	&& rm /tmp/chromedriver_linux64.zip \
	&& mv /opt/chromedriver /opt/chromedriver-$CHROME_DRIVER_VERSION \
	&& chmod 755 /opt/chromedriver-$CHROME_DRIVER_VERSION \
	&& ln -fs /opt/chromedriver-$CHROME_DRIVER_VERSION /usr/bin/chromedriver

# Xvfb
RUN apt-get update -qqy \
	&& apt-get -qqy install xvfb \
	&& rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# The application's jar file
ARG JAR_FILE=target/screenshotapp-0.0.1-SNAPSHOT.jar

# Add the application's jar to the container
ADD ${JAR_FILE} screenshotapp-demo.jar

# Run the jar file
ENTRYPOINT ["java", "-jar","/screenshotapp-demo.jar"]
