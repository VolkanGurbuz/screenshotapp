package com.volkangurbuz.screenshotapp.services;

import com.volkangurbuz.screenshotapp.config.ScreenshotConfig;
import com.volkangurbuz.screenshotapp.domain.Screenshot;
import com.volkangurbuz.screenshotapp.repositories.ScreenshotConfigRepository;
import com.volkangurbuz.screenshotapp.repositories.ScreenshotRepository;
import com.volkangurbuz.screenshotapp.util.Messages;
import com.volkangurbuz.screenshotapp.util.results.Result;
import groovy.util.logging.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ScreenshotServiceImpl implements ScreenshotService {
  private Logger logger = LoggerFactory.getLogger(ScreenshotServiceImpl.class);

  private final ScreenshotRepository screenshotRepository;
  private final ScreenshotConfigRepository screenshotConfigRepository;

  public ScreenshotServiceImpl(
      ScreenshotRepository screenshotRepository,
      ScreenshotConfigRepository screenshotConfigRepository) {
    this.screenshotRepository = screenshotRepository;
    this.screenshotConfigRepository = screenshotConfigRepository;
  }

  @Override
  public Result addScreenshot(Screenshot screenshot) {
    screenshotRepository.save(screenshot);

    return new Result(true, Messages.ADDED_SUCCESSFUL);
  }

  @Override
  public Result addScreenshotConfig(ScreenshotConfig screenshotConfig) {
    try {
      screenshotConfigRepository.save(screenshotConfig);
      return new Result(true, Messages.ADDED_SUCCESSFUL);
    } catch (Exception e) {
      logger.error(e.toString());
      return new Result(false, Messages.SOMETHING_WRONG + " Fail " + e.toString());
    }
  }
}
