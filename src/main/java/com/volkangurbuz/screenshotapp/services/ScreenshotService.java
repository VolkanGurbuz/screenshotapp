package com.volkangurbuz.screenshotapp.services;

import com.volkangurbuz.screenshotapp.config.ScreenshotConfig;
import com.volkangurbuz.screenshotapp.domain.Screenshot;
import com.volkangurbuz.screenshotapp.util.results.Result;

public interface ScreenshotService {

  Result addScreenshot(Screenshot screenshot);

  Result addScreenshotConfig(ScreenshotConfig screenshotConfig);
}
