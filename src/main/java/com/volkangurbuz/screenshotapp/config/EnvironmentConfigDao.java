package com.volkangurbuz.screenshotapp.config;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class EnvironmentConfigDao {

  private ObjectMapper objectMapper = new ObjectMapper();

  public EnvironmentConfig load(String filePath) throws IOException {
    return objectMapper.readValue(
        this.getClass().getResourceAsStream(filePath), EnvironmentConfig.class);
  }
}
