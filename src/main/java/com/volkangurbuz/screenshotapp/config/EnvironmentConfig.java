package com.volkangurbuz.screenshotapp.config;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EnvironmentConfig {

  private List<ScreenshotConfig> screenhooters;
}
