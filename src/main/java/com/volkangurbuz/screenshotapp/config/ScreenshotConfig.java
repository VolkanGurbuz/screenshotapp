package com.volkangurbuz.screenshotapp.config;

import lombok.Getter;
import lombok.Setter;
import org.bson.codecs.pojo.annotations.BsonId;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "screenshotconfig")
public class ScreenshotConfig {
  @BsonId private String id;
  private String name;
  private String url;
  private int intervalSec;
  private String widthScript;
  private String heightScript;
  private boolean active;
}
