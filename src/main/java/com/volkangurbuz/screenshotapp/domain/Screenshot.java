package com.volkangurbuz.screenshotapp.domain;

import com.volkangurbuz.screenshotapp.config.ScreenshotConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.bson.codecs.pojo.annotations.BsonId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.ZonedDateTime;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "screenshot")
public class Screenshot {
  @BsonId private String screenshotId;
  private String date;
  private ZonedDateTime time;
  private ScreenshotConfig screenshotConfig;
  private String imageContent;
}
