package com.volkangurbuz.screenshotapp.controller;

import com.volkangurbuz.screenshotapp.config.ScreenshotConfig;
import com.volkangurbuz.screenshotapp.domain.Screenshot;
import com.volkangurbuz.screenshotapp.services.ScreenshotService;
import com.volkangurbuz.screenshotapp.util.results.Result;
import groovy.util.logging.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
@RequestMapping("/screenshot")
public class ScreenshotController {

  private final ScreenshotService screenshotService;
  private Logger logger = LoggerFactory.getLogger(ScreenshotController.class);

  public ScreenshotController(ScreenshotService screenshotService) {
    this.screenshotService = screenshotService;
  }

  @ResponseStatus(HttpStatus.CREATED)
  @GetMapping("/add")
  public String addScreenshoot(@ModelAttribute Screenshot screenshot, Model model) {
    try {
      Result result = screenshotService.addScreenshot(screenshot);
      String resultMessage = result.getMessage();
      model.addAttribute("message", resultMessage);
      return "screenshottest";
    } catch (Exception e) {
      logger.error(e.toString());
    }
    return "error";
  }

  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping("/addconfig")
  public String addScreenshotConfig(
      @ModelAttribute ScreenshotConfig screenshotConfig, Model model) {
    try {
      Result result = screenshotService.addScreenshotConfig(screenshotConfig);
      model.addAttribute("message", result.getMessage());
      return "screenshot";
    } catch (Exception e) {
      logger.error(e.toString());
    }
    return "error";
  }

  @ResponseStatus(HttpStatus.CREATED)
  @GetMapping("/addconfig")
  public String getScreenshot(@ModelAttribute ScreenshotConfig screenshotConfig, Model model) {
    model.addAttribute("screenshotconfig", screenshotConfig);
    return "addconfig";
  }
}
