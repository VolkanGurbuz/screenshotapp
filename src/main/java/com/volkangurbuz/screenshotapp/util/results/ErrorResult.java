package com.volkangurbuz.screenshotapp.util.results;

public class ErrorResult extends Result {

  public ErrorResult(boolean isResult, String message) {
    super(isResult, message);
  }
}
