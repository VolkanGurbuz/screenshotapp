package com.volkangurbuz.screenshotapp.util.results;

public class SuccessResult extends Result {

  public SuccessResult(boolean isSuccess, String message) {
    super(isSuccess, message);
  }
}
