package com.volkangurbuz.screenshotapp.util;

import io.prometheus.client.Collector;
import io.prometheus.client.Gauge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public final class Size extends Collector {

  private static final Logger logger = LoggerFactory.getLogger(Size.class);

  private static Size instance;

  private final Gauge sizes;

  public Size() {
    sizes =
        Gauge.build().name("_size").help("size of screenshoot").labelNames("name", "type").create();
  }

  public static void size(String name, int number) {
    instance.sizes.labels(name, "").set(number);
  }

  public static void size(String name, String type, int number) {
    instance.sizes.labels(name, type).set(number);
  }

  public static Size getInstance() {

    if (instance == null) {

      logger.debug("Create new size metrics");
      instance = new Size();
    }
    return instance;
  }

  @Override
  public List<MetricFamilySamples> collect() {
    return sizes.collect();
  }
}
