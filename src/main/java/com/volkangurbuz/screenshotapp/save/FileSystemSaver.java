package com.volkangurbuz.screenshotapp.save;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class FileSystemSaver {
  private static final Logger logger = LoggerFactory.getLogger(FileSystemSaver.class);

  public static final DateTimeFormatter DAY_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.of("UTC"));

  private final String saveFolder = "temp";

  public void save(byte[] screenshot, String name) {
    try {
      String folder =
          saveFolder + File.separator + name + File.separator + DAY_FORMATTER.format(Instant.now());

      String fileName = Instant.now() + ".png";

      BufferedImage image = ImageIO.read(new ByteArrayInputStream(screenshot));
      new File(folder).mkdirs();
      ImageIO.write(image, "PNG", new File(folder + fileName));
      logger.info("saved " + fileName);
    } catch (Exception e) {
      logger.error("SaveAction: " + e);
    }
  }
}
