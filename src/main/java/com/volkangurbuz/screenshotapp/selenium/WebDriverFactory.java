package com.volkangurbuz.screenshotapp.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Collections;

public class WebDriverFactory {

  private WebDriverFactory() {}

  public static WebDriver create() {
    // using on docker image
    /*    File chromeDriver = new File("/usr/bin/chromedriver");
    System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());*/
    ChromeOptions chromeOptions = new ChromeOptions();
    chromeOptions.setBinary("/usr/bin/google-chrome");
    chromeOptions.addArguments("--no-sandbox");
    chromeOptions.addArguments("start-maximized");
    chromeOptions.setExperimentalOption(
        "excludeSwitches", Collections.singletonList("enable-automation"));
    chromeOptions.setExperimentalOption("useAutomationExtension", false);
    chromeOptions.addArguments("--disable-extensions");
    chromeOptions.addArguments("--disable-gpu");
    chromeOptions.addArguments("--headless"); // !!!should be enabled for Jenkins
    chromeOptions.addArguments("--disable-dev-shm-usage"); // !!!should be enabled for Jenkins
    chromeOptions.addArguments("--window-size=1920x1080");
    return new ChromeDriver(chromeOptions);
  }
}
