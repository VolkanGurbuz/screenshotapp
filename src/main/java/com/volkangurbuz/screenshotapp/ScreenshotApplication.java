package com.volkangurbuz.screenshotapp;

import com.volkangurbuz.screenshotapp.config.EnvironmentConfig;
import com.volkangurbuz.screenshotapp.config.EnvironmentConfigDao;
import com.volkangurbuz.screenshotapp.config.ScreenshotConfig;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@SpringBootApplication
public class ScreenshotApplication implements CommandLineRunner {

  private ScheduledExecutorService scheduledExecutorService;

  public static void main(String[] args) throws IOException {
    SpringApplication.run(ScreenshotApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {

    EnvironmentConfig environmentConfig =
        new EnvironmentConfigDao().load("/screenshoter-config.json");

    List<ScreenshotConfig> screenshotConfigList =
        environmentConfig.getScreenhooters().stream()
            .filter(ScreenshotConfig::isActive)
            .collect(Collectors.toList());

    scheduledExecutorService = Executors.newScheduledThreadPool(screenshotConfigList.size());

    screenshotConfigList.forEach(
        screenshotConfig -> {
          Screenshoter screenshoter = getScreenshooter(screenshotConfig);
          (scheduledExecutorService)
              .scheduleWithFixedDelay(
                  screenshoter, 0, screenshotConfig.getIntervalSec(), TimeUnit.SECONDS);
        });
  }

  private Screenshoter getScreenshooter(ScreenshotConfig screenshotConfig) {
    Screenshoter screenshoter = new Screenshoter();
    screenshoter.setScreenshotConfig(screenshotConfig);
    return screenshoter;
  }
}
