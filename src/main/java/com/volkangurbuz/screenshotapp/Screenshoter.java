package com.volkangurbuz.screenshotapp;

import com.volkangurbuz.screenshotapp.config.ScreenshotConfig;
import com.volkangurbuz.screenshotapp.save.FileSystemSaver;
import com.volkangurbuz.screenshotapp.selenium.WebDriverFactory;
import io.prometheus.client.Counter;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
public class Screenshoter implements Runnable {

  private static final Logger logger = LoggerFactory.getLogger(FileSystemSaver.class);
  private WebDriver webDriver;
  private ScreenshotConfig screenshotConfig;
  private DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());
  private FileSystemSaver fileSystemSaver = new FileSystemSaver();
  private int howMany = 0;
  private static final Counter requests =
      Counter.build().name("screenshot_requests_total").help("Total requests.").register();

  private static final Counter requestsFail =
      Counter.build().name("screenshot_fail_requests_total").help("Total requests.").register();

  @Override
  public void run() {
    try {
      if (webDriver == null) {
        webDriver = WebDriverFactory.create();
      }
      webDriver.get(screenshotConfig.getUrl());

      doScreenShot();

    } catch (Exception e) {
      logger.error("run " + screenshotConfig.getName() + " screenshoter failed!" + e);
      logger.info("Size fail counter: " + requestsFail.get());
      requestsFail.inc();
      if (webDriver != null) {
        logger.warn("resetting driver");
        webDriver = null;
      }
    }
  }

  private void doScreenShot() {

    requests.inc();
    logger.info("Size counter: " + requests.get());
    fileSystemSaver.save(
        ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES),
        screenshotConfig.getName());
  }
}
