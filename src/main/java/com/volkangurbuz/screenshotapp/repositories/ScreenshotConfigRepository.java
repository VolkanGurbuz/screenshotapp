package com.volkangurbuz.screenshotapp.repositories;

import com.volkangurbuz.screenshotapp.config.ScreenshotConfig;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ScreenshotConfigRepository extends MongoRepository<ScreenshotConfig, String> {}
