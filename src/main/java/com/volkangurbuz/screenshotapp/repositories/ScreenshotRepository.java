package com.volkangurbuz.screenshotapp.repositories;

import com.volkangurbuz.screenshotapp.domain.Screenshot;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ScreenshotRepository extends MongoRepository<Screenshot, String> {}
